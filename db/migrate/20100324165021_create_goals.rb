class CreateGoals < ActiveRecord::Migration
  def self.up
    create_table :goals do |t|
      t.string :description
      t.date :term
      t.boolean :ifprivate
      t.integer :goal_type
      t.references :user

      t.timestamps
    end
  end

  def self.down
    drop_table :goals
  end
end
