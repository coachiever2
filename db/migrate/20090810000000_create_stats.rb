class CreateStats < ActiveRecord::Migration
  def self.up
    create_table :stats do |t|
      t.string    :uid
      t.integer   :action
      t.integer   :extra
      t.timestamps
    end

  end

  def self.down
    drop_table :stats
  end
end
