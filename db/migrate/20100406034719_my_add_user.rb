class MyAddUser < ActiveRecord::Migration
  def self.up
    add_column :users, :permission,:integer
  end
  
  def self.down
    remove_column :users, :permission
  end
end
