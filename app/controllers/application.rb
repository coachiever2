# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  def add_stat(uid, action, extra = 0)
     myParams = {
                :uid               => uid,
                :action            => action,
                :extra             => extra,
                }
      @stat = Stat.new(myParams)
      @stat.save  

      case action
        when INSTALLED
          debug_message ("STAT UPDATED: #{uid} installed application")
        when PERMISSION 
          debug_message ("STAT UPDATED: #{uid} gave permission")
        when DELETED
          debug_message ("STAT UPDATED: #{uid} deleted application")
        when INVITED_NUMBER
          debug_message ("STAT UPDATED: #{uid} invited #{extra} friends")
        when CAME_FIRST_TIME
          case extra
            when FROM_DIRECT
              debug_message ("STAT UPDATED: #{uid} came by direct link")
            when FROM_INVITATION
              debug_message ("STAT UPDATED: #{uid} came trough invitation")
            when FROM_FEED
              debug_message ("STAT UPDATED: #{uid} came from feeds")
            when FROM_SOURCE1
              debug_message ("STAT UPDATED: #{uid} came from FROM_SOURCE1")
            when FROM_SOURCE2 
              debug_message ("STAT UPDATED: #{uid} came from FROM_SOURCE2")
            when FROM_SOURCE3
              debug_message ("STAT UPDATED: #{uid} came from FROM_SOURCE3")
          end
      end
      
  end

  
  def source_statistic
    if (params[:f]) && (params[:f].to_i != 0)
      add_stat(0, VISITED, params[:f] ? params[:f].to_i : 0)
    end         
  end
  
  def debug_message (message,is_not_error = true)
    if Debug_message_on
      if !is_not_error
        p "> > > DEBUG ERROR:"
      end
      p " > > > > >" * 5
      p "          " + message
      p " > > > > >" * 5
    end
  end 
  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
 # protect_from_forgery # :secret => '55d95b70ce63aa969989721ea5bbbb23'
  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  # filter_parameter_logging :password
end
