class UserController < ApplicationController
  ensure_application_is_installed_by_facebook_user
  before_filter :source_statistic 
  def index
 
    #adding invited users to DB
    unless params[:ids].blank?      
      debug_message ("Friends were invited: #{params[:ids].size} by user #{session[:facebook_session].user.uid}")
      #add to statistic 
      add_stat(0, INVITED_NUMBER, params[:ids].size)      
    end
  
    @userF = session[:facebook_session].user
    
    visited = Stat.find(:first, :conditions => ["uid = ? AND action = ?", session[:facebook_session].user.uid, INSTALLED])
    if !visited
      add_stat(session[:facebook_session].user.uid, INSTALLED)
    end

    @fbuser = User.find(:first, :conditions => ["uid = ?", @userF.uid])
    unless @fbuser != nil || @userF.has_permission?("publish_stream") 
       render :action => "permission"    
    end
    
    if @fbuser == nil
      #gathering other params
      myParams = {
                    :uid               => @userF.uid,
                    :first_name        => @userF.first_name,
                    :second_name         => @userF.last_name
                  }
      @fbuser = User.new(myParams)
      add_stat(@fbuser.uid, PERMISSION)
      @fbuser.save
    end

    @my_goals = Goal.find_all_by_user_id(@fbuser.id,:order => "created_at DESC")

    z = @userF.friends.map {|val| val.uid}
    @all_my_friends_in_db = User.find_all_by_uid(z)
    Rails.logger.warn "#{@all_my_friends_in_db.inspect}"

    z2 = @all_my_friends_in_db.map {|val| val.id}
    @goals_friends = Goal.find_all_by_user_id(z2, :order => "created_at DESC")
    Rails.logger.warn "#{@goals_friends.inspect}"
  end
  
  def mygoals
  end

  def help
  end

  def friends_goals
  end
  
  def all_goals
    @goals = Goal.find (:all, :order => "created_at DESC")
    p @goals
  end
    
  def invite
  end
  
  def permission
    if @userF.has_permission?("publish_stream") 
      redirect_to index_url   
    end
  end
  

end
