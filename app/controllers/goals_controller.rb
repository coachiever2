class GoalsController < ApplicationController
  # GET /goals
  # GET /goals.xml
  layout "user"
  ensure_application_is_installed_by_facebook_user
  before_filter :source_statistic 
  def index
    @goals = Goal.find(:all, :order => "created_at DESC")
  end

  # GET /goals/1
  # GET /goals/1.xml
  def show
    @goal = Goal.find(params[:id])
  end

  # GET /goals/new
  # GET /goals/new.xml
  def new
    @goal = Goal.new
  end

  # GET /goals/1/edit
  def edit
    @goal = Goal.find(params[:id])
  end

  # POST /goals
  # POST /goals.xml
  def create
    @goal = Goal.new(params[:goal])
    @fbuser = User.find(:first, :conditions => ["uid = ?", session[:facebook_session].user.uid])

    @goal.user = @fbuser

    if @goal.save
      flash[:notice] = 'Goal was successfully created.'
      begin
        session_new = Facebooker::Session.create
        link = "http://apps.facebook.com/coachiever/"
        @userFB = Facebooker::User.new(@fbuser.uid, session_new)
        publishing(@userFB,@userFB,"has set goal: #{@goal.description}. It should be done till #{@goal.term}.","Watch your friend's goals",link,"See all friend's goals. Set your goals, put them on your profile, let your friends to support you and support your friends.",link,'Help your friend with their goals - write someting in comment.','image',"http://www.pppretirementplans.com/images/successman2.jpg",link)
        add_stat(@fbuser.uid, PUBLISHED)
      rescue
      end
    end
    
    redirect_to index_url
    
  end

  # PUT /goals/1
  # PUT /goals/1.xml
  def update
    @goal = Goal.find(params[:id])

    
      if @goal.update_attributes(params[:goal])
        flash[:notice] = 'Goal was successfully updated.'
        redirect_to index_url
        return
      end

  end

  # DELETE /goals/1
  # DELETE /goals/1.xml
  def destroy
    @goal = Goal.find(params[:id])
    @goal.destroy
    redirect_to index_url

  end

   def publishing(user_from,user_to,main_message,action_link_text,action_link_href,attachment_name,attachment_href,attachment_caption,media_type,media_src,media_href)
    user_from.publish_to(user_to,
        :message => main_message,
        :action_links => [
                {
                :text => action_link_text,
                :href => action_link_href}
                ],
        :attachment => {
                :name => attachment_name,
                :href => attachment_href,
                :caption => attachment_caption,

                :media => [
                        {
                          :type => media_type,
                          :src =>  media_src,
                          :href => media_href
                        }

                       ]})
  end
end
