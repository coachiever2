class Goal < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :description ,:term

  GoalType = {
    0 => 'Career & Work',
    1 => 'Money',
    2 => 'Health',
    3 => 'Education and development',
    4 => 'Social life',
    5 => 'Home & family',
    6 => 'Emotions',
    7 => 'Character',
    8 => 'Life purpose',
    9 => 'Spirutal development',
    10 => 'Other'
  }
end
