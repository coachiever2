module UserHelper
  def tabs_styling(tab,current_action)
    html =""
    if (tab==current_action)
      html << " class='current' "
    end    
  end

  def show_goals(name_of_list,goals)
  html = "" 
  unless goals.blank?
  html << %{

    <h2>#{name_of_list}</h2>
    <div style=" width: 100%;">
      <table>}
          for goal in goals 
          html << %{
          <tr>
            <td style="border: 1px solid rgb(98, 122, 173);" style="vertical-align:top; border: 1px solid rgb(98, 122, 173);"><fb:profile-pic size="normal" uid="#{goal.user.uid}" width="32" linked="true" /> </td>
            <td style="border: 1px solid rgb(98, 122, 173);"> #{Goal::GoalType[goal.goal_type]}</td>
            <td style="border: 1px solid rgb(98, 122, 173);" width="150"><a href="#{onegoal_url}?id=#{goal.id}"> #{goal.description}</a></td>
            
            <td style="border: 1px solid rgb(98, 122, 173); width:70px">#{goal.term}</td>
          </tr>}
         end 
      html << %{
      </table>

    </div>
}
  end

  end

end
