ActionController::Routing::Routes.draw do |map|
  map.resources :goals

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing the them or commenting them out if you're using named routes and resources.
  map.goal_show   '/goal_show', :controller => 'goals', :action => 'index'
  map.goal_new    '/goal_new', :controller => 'goals', :action => 'new'
  map.onegoal     '/onegoal', :controller => 'goals', :action => 'show'
  map.destroy     '/destroy', :controller => 'goals', :action => 'destroy'
  map.edit_goal   '/edit_goal', :controller => 'goals', :action => 'edit'
  
  map.stat    '/stat', :controller => 'admin', :action => 'stat'
  map.signout '/signout', :controller => 'admin', :action => 'sign_out'
  map.auth    '/auth', :controller => 'admin', :action => 'auth'

  map.fbdel    '/fbdel', :controller => 'admin', :action => 'fbdelete'

  map.index       '/index', :controller => 'user', :action => 'index'
  map.all_goals   '/all_goals', :controller => 'user', :action => 'all_goals'
  map.invite      '/invite', :controller => 'user', :action => 'invite'
  map.help        '/help', :controller => 'user', :action => 'help'


  map.root :controller => "user"
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
